#Provisionar entorno de producción
ansible-playbook playbook.yml -i inventory/hosts -l "prod"
#Provisionar entorno de devel
ansible-playbook playbook.yml -i inventory/hosts -l "devel"
#Provisionar ambos entorno
ansible-playbook playbook.yml -i inventory/hosts
